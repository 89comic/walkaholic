package msdl.walkaholic;

import msdl.walkaholic.database.DBConnect;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class ActMain extends Activity {
	Button btnLogin, btnSignup;
	static DBConnect DBC;
	public static int PID;
	String id, pw;
	
	//Tested
	//+Plus
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	requestWindowFeature(Window.FEATURE_NO_TITLE); //타이틀바 없애기
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);  //login.xml을 액티비티에 채움
        DBC = new DBConnect();
        btnLogin = (Button)findViewById(R.id.Btn1);
        btnLogin.setOnClickListener(new OnClickListener(){ //Login 버튼 클릭시 입력한 id,pw 변수에 저장
			public void onClick(View v) {
				// TODO Auto-generated method stub
				id = ((EditText)findViewById(R.id.txtID)).getText()+"";
				pw = ((EditText)findViewById(R.id.txtPW)).getText()+"";
				Intent intent = new Intent(ActMain.this,ActWalkaholic.class);
    		    startActivityForResult(intent,1);
				/*
				PID = DBC.getPID(id, pw);
				if(PID != -1){
					Intent intent = new Intent(ActMain.this,ActWalkaholic.class);
	    		    startActivityForResult(intent,1);
	    		    finish();
				}*/
			}
        });
       
        btnSignup = (Button)findViewById(R.id.btnSign);
        btnSignup.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ActMain.this,ActSignup.class);
    		    startActivityForResult(intent,1);
    		    finish();
			}
        });
    }
}
