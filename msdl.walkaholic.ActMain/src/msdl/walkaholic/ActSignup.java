package msdl.walkaholic;

import msdl.walkaholic.database.DBConnect;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class ActSignup extends Activity{
	DBConnect DBC = ActMain.DBC;
	public void onCreate(Bundle savedInstanceState){
		 super.onCreate(savedInstanceState);
	     setContentView(R.layout.signup);
	     
	     Button btnSign = (Button)findViewById(R.id.btnSignUp);
	     btnSign.setOnClickListener(new OnClickListener(){

			public void onClick(View v) {
				// TODO Auto-generated method stub
				String id = ((EditText)findViewById(R.id.eT1)).getText() + "";
				String pw = ((EditText)findViewById(R.id.eT2)).getText() + "";
				String age = ((EditText)findViewById(R.id.eT3)).getText() + "";
				boolean gender = ((RadioButton)findViewById(R.id.male)).isChecked();
				DBC.newUser(id, pw, age, gender);
			}
        });
	}
}
