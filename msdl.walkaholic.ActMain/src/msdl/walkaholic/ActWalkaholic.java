package msdl.walkaholic;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DecimalFormat;

import msdl.walkaholic.bluetooth.BluetoothAct;
import msdl.walkaholic.bluetooth.BluetoothConnection;
import msdl.walkaholic.database.DBConnect;
import msdl.walkaholic.database.WalkEntity;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActWalkaholic extends Activity {
	TextView txt1;
	TextView txt2;
	String str;
	String Status_Message;
	BluetoothConnection bCon;
	BlueAct bAct;
	
	IntervalGraph iGraph;
	WidGraph wGraph;
	WidStepLight wStepLight;
	
	//status of step
	WalkEntity wEnt;
	final int STEP_LEFT=1, STEP_RIGHT=2;
	boolean isStepL=true;
	boolean isStepR=true;
	int	isStep = STEP_LEFT;
	double stepL=0;
	double stepR=0;

	int cntA=0, cntB=0;
	
	//formatting
	DecimalFormat df = new DecimalFormat("####.##");
	String Wd_stepLR;
	String Wd_stepRL;
	String Wd_stepInterval;
	String Wd_strideL;
	String Wd_strideR;
	String Wd_stepCnt;
	
	//file
	String str_Path_Full = Environment.getExternalStorageDirectory().getAbsolutePath();
	File file_A = new File(str_Path_Full+"/Data_A.txt");
	File file_B = new File(str_Path_Full+"/Data_B.txt");
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setFullscreen();
        setNoTitle();
        
        bAct = new BlueAct();
        wEnt = new WalkEntity();
        
        LinearLayout frame = new LinearLayout(this);
        frame.setOrientation(LinearLayout.VERTICAL);
        frame.setBackgroundColor(Color.BLACK);
      
        Button capture_btn = new Button(this);
        capture_btn.setText("Capture Data");
        iGraph = new IntervalGraph(this, getWindowManager().getDefaultDisplay().getWidth());
        wGraph = new WidGraph(this, getWindowManager().getDefaultDisplay().getWidth());
        wStepLight = new WidStepLight(this, getWindowManager().getDefaultDisplay().getWidth());
        txt1 = new TextView(this);
        txt1.setHeight(getWindowManager().getDefaultDisplay().getHeight()/4);
        txt1.setPadding(30, 30, 30, 5);
        txt1.setTextColor(Color.YELLOW);
        txt2 = new TextView(this);
        txt2.setGravity(Gravity.CENTER);
        //txt2.setHeight(getWindowManager().getDefaultDisplay().getHeight()/20);
        txt2.setPadding(30, 30, 30, 5);
        txt2.setTextColor(Color.YELLOW);
        txt2.setTextSize(15);
        
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		params.height = getWindowManager().getDefaultDisplay().getHeight()/3;
		
		LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		params2.height = getWindowManager().getDefaultDisplay().getHeight()/11;
		
        frame.addView(txt1);
        frame.addView(capture_btn);
        frame.addView(iGraph,params2);
        frame.addView(wStepLight,params);
		frame.addView(txt2);
        frame.addView(wGraph,params);
        
        setContentView(frame);
   
        txt1.setText("Walkaholic");
        txt2.setText("<Realtime Voltage Graph>");
        bCon = new BluetoothConnection(this, bAct);
        if(bCon.isBluetoothAvailable()){
	        if(bCon.ConnectBluetooth("MSDL")){
	        	bCon.StartBluetooth();
	        }
        }
        
        capture_btn.setOnClickListener(new Button.OnClickListener(){
			public void onClick(View v){
				try {
					String dataA = new String();
					String dataB = new String();
					String tmp_str = new String();
					for(Integer i : wGraph.getDataA()){
						tmp_str = i+" ";
						dataA += tmp_str; 
					}
					for(Integer i : wGraph.getDataB()){
						tmp_str = i+" ";
						dataB += tmp_str; 
					}
							
					//Log.e("dataA",wGraph.getDataA()+"");
					//Log.e("dataB",wGraph.getDataB()+"");
					FileOutputStream fosA = new FileOutputStream(file_A);
					FileOutputStream fosB = new FileOutputStream(file_B);
					fosA.write(dataA.getBytes());
					fosB.write(dataB.getBytes());
					fosA.close();
					fosB.close();
					
				} catch (Exception e) {;}
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_act_main, menu);
        MenuItem item=menu.add(0,1,0,"Reset");
        MenuItem item2=menu.add(0,2,0,"Save");
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case 1:
			wEnt.reset();
			return true;
		case 2:
			Save();
			return true;
		}
		return false;
	}
    
    public void Save(){
    	DBConnect DBC = ActMain.DBC;
    	DBC.setWalkEntity(wEnt);
    }
    final Handler handler = new Handler()  {
    	
    	public void handleMessage(Message msg)  {
    		Wd_stepLR = df.format(wEnt.getWd_stepLR()).toString();
    		Wd_stepRL = df.format(wEnt.getWd_stepRL()).toString();
    		Wd_stepInterval = df.format(wEnt.getWd_stepInterval()).toString();
    		Wd_strideL = df.format(wEnt.getWd_strideL()).toString();
    		Wd_strideR = df.format(wEnt.getWd_strideR()).toString();
    		Wd_stepCnt = df.format(wEnt.getWd_stepCnt()).toString();
    		
    		Status_Message = new String("Walkaholic // MSDL is Connected\n");
    		Status_Message += "StepLR : " + Wd_stepLR + "\n";
    		Status_Message += "StepRL : " + Wd_stepRL + "\n";
    		Status_Message += "StepInterval : " +Wd_stepInterval + "\n";
    		Status_Message += "StrideL : " + Wd_strideL + "\n";
    		Status_Message += "StrideR : " + Wd_strideR + "\n";
    		Status_Message += "StepCnt : " + Wd_stepCnt + "\n";
    		
    		txt1.setText(Status_Message);
    		txt1.setTextSize(16);
 			
    		//wGraph.run();
    		wStepLight.invalidate();
    		wGraph.invalidate();
    		iGraph.invalidate();
    		super.handleMessage(msg);
    	}


    };
	public void setFullscreen() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
		WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	public void setNoTitle() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	}
	
    public class BlueAct extends BluetoothAct{
    	public BlueAct(){
            str = new String("");
    	}
		@Override
		public void readAct(int bytes, byte[] buffer) {
			// TODO Auto-generated method stub
			try{
				for(int i=0; i<bytes; i++) str += (char)buffer[i];
				System.out.println("Original : " + str);
				
				Message msg =new Message();
				
				String splitStr[] = str.split("/");
				
				for(String splittedString : splitStr){
					int data=0;
					
					try{
						data = Integer.parseInt((splittedString.substring(0, splittedString.length()-1)).trim());
						if(data>600) data = 600;
						if(data<0) data = 0;
					}catch(Exception e){
					}
					
					if(splittedString.equals("")) continue;
					if(splittedString.charAt(splittedString.length()-1) == 'A'){ // 오른발의 상태
						if(data>100){ // data값이 100이상일경우는 발이 아예 띄어진 것으로 생각 
							wStepLight.isStep('A', false);
							isStepR = false;
						}
						else if(data<50){// data값의 역치를 50으로 두어 50이하일경우는 오른발 압력이 가해진 것으로 설정.
							wStepLight.isStep('A', true);
							
							if(!isStepR && isStep == STEP_LEFT){
								wEnt.setWd_strideR((System.currentTimeMillis()-stepR)/1000);
								stepR = System.currentTimeMillis();
								wEnt.setWd_stepLR((stepR - stepL)/1000);  //오른발과 왼발 까지의 차이 
								wEnt.addWalk();  //걸음수 +1 추가
								isStepR = true;
								isStep = STEP_RIGHT;
								
								try{
					    			iGraph.addData(Double.parseDouble(df.format(wEnt.getWd_stepInterval()).toString()));
					    		}catch(Exception e){
					    			iGraph.addData(0);
					    		}
								
							}
						}

						wGraph.addData('A', data);
						str = str.substring(str.indexOf("A")+2);
						System.out.println(cntA++ + ": CutA : " + str);
					}
					else if(splittedString.charAt(splittedString.length()-1) == 'B'){   // 왼발의 상태
						
						if(data>100){
							wStepLight.isStep('B', false);
							isStepL = false;
						}
						else if(data<50){
	
							wStepLight.isStep('B', true);
							if(!isStepL && isStep == STEP_RIGHT){
								wEnt.setWd_strideL((System.currentTimeMillis()-stepL)/1000);
								stepL = System.currentTimeMillis();
								wEnt.setWd_stepRL((stepL - stepR)/1000);
								wEnt.addWalk();
								isStepL = true;
								isStep = STEP_LEFT;
						
								try{
					    			iGraph.addData(Double.parseDouble(df.format(wEnt.getWd_stepInterval()).toString()));
					    		}catch(Exception e){
					    			iGraph.addData(0);
					    		}
								
							}
							
							
						}

						wGraph.addData('B', data);
						
						str = str.substring(str.indexOf("B")+2);
						System.out.println(cntB++ + ": CutB : " + str);
					}
					
					handler.sendMessage(new Message());
				}
				
			}catch(Exception e){
				System.out.println("Read Error : " + e);
			}
		}

		@Override
		public void writeAct(String buffer) {
			// TODO Auto-generated method stub
			
		}
    }
    public void onDestroy(){
    	super.onDestroy();
    	bCon.stop();
    }
    public boolean onKeyDown(int keyCode, KeyEvent event){
		if(keyCode == KeyEvent.KEYCODE_BACK){
			onDestroy();
		}return super.onKeyDown(keyCode, event);
	}
}
