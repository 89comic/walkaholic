package msdl.walkaholic;

import java.util.ArrayList;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class IntervalGraph extends View {
	ArrayList<Integer> mGraph_Data_A; 

	private int startX; 
	private int endX;
	private int startY;
	private int endY; 

	//Appeareance
	Paint mPaintGray;
	Paint mPaintGreen;

	//Holder

	int cntA=0, cntB=0;

	public IntervalGraph(Context context, int Width) {
		super(context);

		//set variable
		startX = Width/20;
		endX = Width - Width/20;
		startY = 10;
		endY = 100;
	
		mGraph_Data_A = new ArrayList<Integer>();

		//Paint Preset
		mPaintGray = new Paint();
		mPaintGray.setStyle(Paint.Style.FILL);
		mPaintGray.setColor(Color.rgb(150, 150, 150));

		mPaintGreen = new Paint();
		mPaintGreen.setStyle(Paint.Style.FILL);
		mPaintGreen.setColor(Color.GREEN);
		mPaintGreen.setStrokeWidth(6);
		mPaintGreen.setAntiAlias(true);

	}

	public void addData(double data){
		if(mGraph_Data_A.size() < 10){
			mGraph_Data_A.add((int)(data*100));
		}
		else{
			mGraph_Data_A.remove(0);
			mGraph_Data_A.add((int)(data*100));
		}
			System.out.println("A : " + data + " / "+ mGraph_Data_A.size());
	}

	public void onDraw(Canvas canvas){
		int canvasWidth = canvas.getWidth();
		//Draw Outline
		for(int i=startX; i<=canvasWidth; i=i+canvasWidth/10)
			canvas.drawLine(i, startY, i, endY, mPaintGray);
		for(int i=startY; i<=endY; i+=30)
			canvas.drawLine(startX, i, endX, i, mPaintGray);

		//Draw Graph
		int mStart_PositionA = endX - mGraph_Data_A.size()*3;
		int mPosition = 0;
		
		mPosition=0;
		try{
			for(int i=0; i<mGraph_Data_A.size(); i++){
				int dataY = endY-(startY + (mGraph_Data_A.get(i) * endY / 200)) +10;
				if(dataY > endY) dataY=endY;
				else if(dataY < startY) dataY=endY;

				int dataY_2 = endY-(startY + (mGraph_Data_A.get(i+1) * endY / 200)) +10;
				if(dataY_2 > endY) dataY_2=startY;
				else if(dataY_2 < startY) dataY_2=endY;

				canvas.drawLine(i*(canvasWidth/10)+startX, dataY, (i+1)*(canvasWidth/10)+startX, dataY_2, mPaintGreen);
			}

		}catch(Exception e){
			System.out.println("A Error :" + cntA++ + e);
		}
	}
}
