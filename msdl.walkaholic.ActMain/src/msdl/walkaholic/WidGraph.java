package msdl.walkaholic;

import java.util.ArrayList;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class WidGraph extends View {
	ArrayList<Integer> mGraph_Data_A; 
	ArrayList<Integer> mGraph_Data_B;
	private int startX; 
	private int endX;
	private int startY;
	private int endY; 
	//Appeareance
	Paint mPaintGray;
	Paint mPaintRed;
	Paint mPaintBlue;

	//Checker for input
	boolean isA = true;
	//Holder

	int cntA=0, cntB=0;

	public WidGraph(Context context, int Width) {
		super(context);

		//set variable
		startX = Width/20;
		endX = Width - Width/20;
		startY = 10;
		endY = 250;

		mGraph_Data_A = new ArrayList<Integer>();
		mGraph_Data_B = new ArrayList<Integer>();

		//Paint Preset
		mPaintGray = new Paint();
		mPaintGray.setStyle(Paint.Style.FILL);
		mPaintGray.setColor(Color.rgb(150, 150, 150));

		mPaintRed = new Paint();
		mPaintRed.setStyle(Paint.Style.FILL);
		mPaintRed.setColor(Color.BLUE);
		mPaintRed.setStrokeWidth(6);
		mPaintRed.setAntiAlias(true);

		mPaintBlue = new Paint();
		mPaintBlue.setStyle(Paint.Style.FILL);
		mPaintBlue.setColor(Color.RED);
		mPaintBlue.setStrokeWidth(6);
		mPaintBlue.setAntiAlias(true);      
	}
	
	public ArrayList<Integer> getDataA(){
		return mGraph_Data_A;
	}
	public ArrayList<Integer> getDataB(){
		return mGraph_Data_B;
	}
	public void addData(char type, int data){
		switch(type){
		case 'A':
			if(isA){
				if(mGraph_Data_A.size() < (endX - startX)/6){
					mGraph_Data_A.add(data);
				}
				else{
					mGraph_Data_A.remove(0);
					mGraph_Data_A.add(data);
				}
				//System.out.println("A : " + data + " / "+ mGraph_Data_A.size());
				isA = false;
				break;
			}
		case 'B':
			if(!isA){
				if(mGraph_Data_B.size() < (endX - startX)/6){
					mGraph_Data_B.add(data);
				}
				else{
					mGraph_Data_B.remove(0);
					mGraph_Data_B.add(data);
				}
				//System.out.println("B : " + data + " / "+ mGraph_Data_B.size());
				isA=true;
				break;
			}
		}
	}

	public void onDraw(Canvas canvas){
		int canvasWidth = canvas.getWidth();
		//Draw Outline
		for(int i=startX; i<=canvasWidth; i=i+canvasWidth/10)
			canvas.drawLine(i, startY, i, endY, mPaintGray);
		for(int i=startY; i<=endY; i+=20)
			canvas.drawLine(startX, i, endX, i, mPaintGray);

		//Draw Graph
		int mStart_PositionA = endX - mGraph_Data_A.size()*6;
		int mStart_PositionB = endX - mGraph_Data_B.size()*6;
		int mPosition = 0;
		try{
			for(int i=mStart_PositionB; i<endX-5; i+=6){
				int dataY = startY + (int)((double)(mGraph_Data_B.get(mPosition++) / 600.0 * endY-10)); 
				if(dataY > endY) dataY=endY;
				else if(dataY < startY) dataY=startY;

				int dataY_2 = startY + (int)((double)(mGraph_Data_B.get(mPosition) / 600.0 * endY-10));
				if(dataY_2 > endY) dataY_2=endY;
				else if(dataY_2 < startY) dataY_2=startY;

				canvas.drawLine(i, dataY, i+5, dataY_2, mPaintRed);
			}
		}catch(Exception e){
			System.out.println("B Error :" + cntB++ + e);
		}
		mPosition=0;
		try{
			for(int i=mStart_PositionA; i<endX-5; i+=6){
				int dataY = startY + (int)((double)(mGraph_Data_A.get(mPosition++) / 600.0 * endY-10));
				if(dataY > endY) dataY=endY;
				else if(dataY < startY) dataY=startY;

				int dataY_2 = startY + (int)((double)(mGraph_Data_A.get(mPosition) / 600.0 * endY-10));
				if(dataY_2 > endY) dataY_2=endY;
				else if(dataY_2 < startY) dataY_2=startY;

				canvas.drawLine(i, dataY, i+5, dataY_2, mPaintBlue);
			}

		}catch(Exception e){
			System.out.println("A Error :" + cntA++ + e);
		}
	}
}
