package msdl.walkaholic;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;

public class WidStepLight extends View{
	private boolean isStepA = false;
	private boolean isStepB = false;
	private int startX; 
	private int endX;
	private int startY;
	private int endY; 
	
	Paint mPaintRed;
	Paint mPaintBlue;
	Paint mPaintRed_B;
	Paint mPaintBlue_B;
	
	Resources res = getResources();
	BitmapDrawable left_red = (BitmapDrawable)res.getDrawable(R.drawable.red_left);
	BitmapDrawable left_blank = (BitmapDrawable)res.getDrawable(R.drawable.blank_left);
	BitmapDrawable right_blue = (BitmapDrawable)res.getDrawable(R.drawable.right_blue);
	BitmapDrawable right_blank = (BitmapDrawable)res.getDrawable(R.drawable.right_blank);
	
	Bitmap lr = left_red.getBitmap();
	Bitmap lb = left_blank.getBitmap();
	Bitmap rb = right_blue.getBitmap();
	Bitmap rl = right_blank.getBitmap();
	public WidStepLight(Context context, int Width) {
		super(context);
		
		startX = Width/20;
		endX = Width - Width/20;
		startY = 40;
		endY = 400;
		    
		// TODO Auto-generated constructor stub
	}
	
	public void isStep(char type, boolean isStepped){
		switch(type){
		case 'A':
			isStepA = isStepped;
			break;
		case 'B':
			isStepB = isStepped;
			break;
		}
	}
	
	public void onDraw(Canvas canvas){
		int canvasWidth = canvas.getWidth();
		if(isStepA)
			canvas.drawBitmap(lr,null, new Rect(startX, startY,(startX+endX)/2-canvasWidth/20, endY),null);
		else
			canvas.drawBitmap(lb,null, new Rect(startX, startY, (startX+endX)/2-canvasWidth/20, endY),null);
			
		if(isStepB)
			canvas.drawBitmap(rb,null, new Rect((startX+endX)/2+canvasWidth/20, startY, endX, endY),null);
		else
			canvas.drawBitmap(rl,null, new Rect((startX+endX)/2+canvasWidth/20, startY, endX, endY),null);
					
	}
}
