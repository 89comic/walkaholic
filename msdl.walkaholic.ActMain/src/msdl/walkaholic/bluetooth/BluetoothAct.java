package msdl.walkaholic.bluetooth;

public abstract class BluetoothAct{
	public abstract void readAct(int bytes, byte buffer[]);
	public abstract void writeAct(String buffer);
	
}
