package msdl.walkaholic.bluetooth;

import android.app.Activity;
import android.bluetooth.*;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class BluetoothConnection{
	private static BluetoothAdapter mBAdapter;
	private static InputStream mBInput;
	private static OutputStream mBOutput;
	private static ConnectThread mConnectThread;
	private static ConnectedThread mConnectedThread;
	private static byte[] buffer = new byte[1024];
	private static int bytes;   
	public BluetoothAct mBAct;
	
	final static UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	public Activity currentActivity;
	
	public BluetoothConnection(Activity currentActivity, BluetoothAct mBAct){
		mBAdapter = BluetoothAdapter.getDefaultAdapter();
		this.mBAct = mBAct;
		this.currentActivity = currentActivity;
	}
	
	public BluetoothConnection(BluetoothAct mBAct){
		mBAdapter = BluetoothAdapter.getDefaultAdapter();
		this.mBAct = mBAct;
		this.currentActivity = null;
	}
	
	public boolean isBluetoothAvailable(){
        if(mBAdapter == null){
        	mkToast("Bluetooth is not Available");
        	return false;
        }
        else{
        	mkToast("Bluetooth is Available");
        	return true;
        }
	}
	
	public boolean ConnectBluetooth(String Bluetooth_Name){
		Set<BluetoothDevice> pairedDevices = mBAdapter.getBondedDevices();
        if(pairedDevices.size() > 0){
        	for(BluetoothDevice device : pairedDevices){
        		//System.out.println(device.getUuids());
        		if(device.getName().equals(Bluetooth_Name)){
        			try{
        				mConnectThread = new ConnectThread(device);
        				mConnectThread.start();
        				mkToast(Bluetooth_Name + " is Connected :D");
        			}catch(Exception e){
        				e.printStackTrace();
        			}
        		}
        	}
        }
        return true;
	}
	public boolean StartBluetooth(){
		try{
			mConnectedThread = new ConnectedThread(mConnectThread.getSocket());
			mConnectedThread.start();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
    public void mkToast(String str){
    	Toast.makeText(currentActivity,  str, Toast.LENGTH_LONG).show();
    }
    
    private class ConnectThread extends Thread{
    	private final BluetoothSocket mmSocket;
    	private final BluetoothDevice mmDevice;
    	public boolean isConnected= false;
    	
    	public ConnectThread(BluetoothDevice device){
    		BluetoothSocket tmp = null;
    		mmDevice = device;
    		
    		try{
    			tmp = device.createRfcommSocketToServiceRecord(SPP_UUID);
    		}
    		catch(IOException e){}
    		
    		mmSocket = tmp;
    	}
    	
    	public void run(){
    		mBAdapter.cancelDiscovery();
    		try{
    			mmSocket.connect();
    			isConnected = true;
    		}catch(IOException e){
    			isConnected = false;
    			try{
    				mmSocket.close();
    			}catch(IOException closeException){}
    			return;
    		}
    	}
    	
    	public BluetoothSocket getSocket(){
    		return mmSocket;
    	}
    	
    	public void cancel(){
    		try {
				mmSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	} 
    }
    private class ConnectedThread extends Thread{
    	private final BluetoothSocket mmSocket; 	
    	
    	public ConnectedThread(BluetoothSocket socket){
    		mmSocket = socket;
    		InputStream tmpIn = null;
    		OutputStream tmpOut = null;
    		try{
    			tmpIn = socket.getInputStream();
    			tmpOut = socket.getOutputStream();
    		}
    		catch(IOException e){}
    		mBInput = tmpIn;
    		mBOutput = tmpOut;
    	}
    	
    	public void run(){
    		while(true){
    			try{
    				bytes = mBInput.read(buffer);
    				mBAct.readAct(bytes, buffer);
    				buffer = new byte[1024];
    			}
    			catch(Exception e) { }
    		}
    	}
    	
    	public void cancel(){
    		try {
				mmSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}	
    }
    
    public synchronized void stop(){
    	if(mConnectThread != null){
    		mConnectThread.cancel();
    		mConnectThread.resume();
    		//mConnectThread.destroy();
    		//mConnectThread = null;
    	}
    	if(mConnectedThread != null){
    		mConnectedThread.cancel();
    		mConnectThread.resume();
    		//mConnectThread.destroy();
    		//mConnectedThread = null;
    	}
    }
}
