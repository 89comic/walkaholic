package msdl.walkaholic.database;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.StrictMode;

@SuppressLint("NewApi")
public class DBConnect {
	private String sUrl = "http://220.69.203.184/Walkaholic/WebContent/";
	
	private String Connect(ArrayList<NameValuePair> nameValuePairs){ 
		InputStream is = null;
		String result = "";
		String url = sUrl + "DB_Connect.jsp";
		
		//우회
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		// 요청한 url 문자열로 지정
		HttpClient httpclient = new DefaultHttpClient();
		// DefaultHttpClient 이용해서 http클라이언트 생성
		try {
	
			HttpParams params = httpclient.getParams();
			// 파라미터를 얻어와서
			HttpConnectionParams.setConnectionTimeout(params, 5000);
			// 5초 이상 연결이 안되면 끊어지게 (milliseconds 단위)
			
			HttpPost httppost = new HttpPost(url);
			// Post 방식의 요청
			
			UrlEncodedFormEntity entityRequest = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
			// 다국어 처리
			httppost.setEntity(entityRequest);
			// 엔티티 지정
			
			HttpResponse response = httpclient.execute(httppost);
			// 실행하고 결과 response로 받아오기
			
			HttpEntity entityResponse = response.getEntity();
			// 엔티티 얻어오기
			is = entityResponse.getContent();
			// 응답된 데이터를 읽을수있는 입력스트림 넘어옴
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "euc-kr"), 8);
			// 인코딩 처리 버퍼드리더 얻어옴
			StringBuilder sb = new StringBuilder();
			String line = null;
			boolean split = false;
			
			while ((line = reader.readLine()) != null){
				if(line.equals("") == false){
					if (line.trim().substring(0, 1).equals("[")){
						split = true;
					}
				}
				else split = false;
				
				if (split==true) sb.append(line.trim()).append("\n");
			}
			is.close();
			// 인풋스트림 닫음
			result = sb.toString();
			System.out.println(result);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		} finally {
			httpclient.getConnectionManager().shutdown();
			// httpclinet 닫음
		}
		return result;
	}
	
	public int getPID(String id, String pw){ //
		int nPID=-1;
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>(); // 패러매터
		param.add(new BasicNameValuePair("type", "pid"));
		param.add(new BasicNameValuePair("id", id));
		param.add(new BasicNameValuePair("pw", pw));
		String JSONQuery = Connect(param);
		
		try{
			JSONArray ja = new JSONArray(JSONQuery);
			for(int i=0; i<ja.length(); i++){
				JSONObject QObj = ja.getJSONObject(i);
				nPID = QObj.getInt("p_id");
			}
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
		return nPID;
	}
	
	public void setWalkEntity(WalkEntity wEnt){
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>(); // 패러매터
		param.add(new BasicNameValuePair("type", "data"));
		param.add(new BasicNameValuePair("p_id", wEnt.getP_id()+""));
		param.add(new BasicNameValuePair("user_p_id", wEnt.getUser_p_id()+""));
		param.add(new BasicNameValuePair("wd_stepLR", wEnt.getWd_stepLR()+""));
		param.add(new BasicNameValuePair("wd_stepRL", wEnt.getWd_stepRL()+""));
		param.add(new BasicNameValuePair("wd_strideL", wEnt.getWd_strideL()+""));
		param.add(new BasicNameValuePair("wd_strideR", wEnt.getWd_strideR()+""));
		param.add(new BasicNameValuePair("wd_stepInterval", wEnt.getWd_stepInterval()+""));
		param.add(new BasicNameValuePair("wd_stepCnt", wEnt.getWd_stepCnt()+""));
		param.add(new BasicNameValuePair("wd_date", wEnt.getWd_date()+""));
		
		Connect(param);
	}
	
	public void newUser(String id, String pw, String age, boolean gender){
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>(); // 패러매터
		param.add(new BasicNameValuePair("type", "new"));
		param.add(new BasicNameValuePair("u_id", id));
		param.add(new BasicNameValuePair("u_pw", pw));
		param.add(new BasicNameValuePair("ui_age", age));
		param.add(new BasicNameValuePair("ui_gender", gender?"M":"F"));
		
		Connect(param);
	}
}
