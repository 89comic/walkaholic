package msdl.walkaholic.database;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import msdl.walkaholic.ActMain;

public class WalkEntity {
	private int p_id;
	private int user_p_id;
	private double wd_stepLR;
	private double wd_stepRL;
	private double wd_strideL;
	private double wd_strideR;
	private double wd_stepInterval;
	private int wd_stepCnt;
	private String wd_date;
	
	public void reset(){
		wd_stepLR = 0;
		wd_stepRL = 0;
		wd_strideL = 0;
		wd_strideR = 0;
		wd_stepInterval = 0;
		wd_stepCnt = 0;
	}
	public WalkEntity(){
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
		Date current = new Date();
		wd_date = formater.format(current);
		wd_stepCnt=0;
		p_id = ActMain.PID;
	}
	public void addWalk(){
		wd_stepCnt++;
	}
	public int getP_id() {
		return p_id;
	}
	public void setP_id(int p_id) {
		this.p_id = p_id;
	}
	public int getUser_p_id() {
		return user_p_id;
	}
	public void setUser_p_id(int user_p_id) {
		this.user_p_id = user_p_id;
	}
	public double getWd_stepLR() {
		return wd_stepLR;
	}
	public void setWd_stepLR(double wd_stepLR) {
		this.wd_stepLR = wd_stepLR;
	}
	public double getWd_stepRL() {
		return wd_stepRL;
	}
	public void setWd_stepRL(double wd_stepRL) {
		this.wd_stepRL = wd_stepRL;
	}
	public double getWd_strideL() {
		return wd_strideL;
	}
	public void setWd_strideL(double wd_strideL) {
		this.wd_strideL = wd_strideL;
	}
	public double getWd_strideR() {
		return wd_strideR;
	}
	public void setWd_strideR(double wd_strideR) {
		this.wd_strideR = wd_strideR;
	}
	public double getWd_stepInterval() {
		wd_stepInterval = Math.abs(wd_stepLR-wd_stepRL);
		return wd_stepInterval;
	}
	public void setWd_stepInterval(double wd_stepInterval) {
		this.wd_stepInterval = wd_stepInterval;
	}
	public int getWd_stepCnt() {
		return wd_stepCnt;
	}
	public void setWd_stepCnt(int wd_stepCnt) {
		this.wd_stepCnt = wd_stepCnt;
	}
	public String getWd_date() {
		return wd_date;
	}
	public void setWd_date(String wd_date) {
		this.wd_date = wd_date;
	}
}
